// Free Disassembler and Assembler -- Demo program
//
// Copyright (C) 2001 Oleh Yuschuk
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#define STRICT
#define MAINPROG                       // Place all unique variables here

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <float.h>
#pragma hdrstop

#include "disasm.h"

int assemble( uint8_t * asm_str ) {

    uint8_t error[ 1024 ] = { '\0' };
    t_asmmodel model;
    int i=0, r=0;

    r = Assemble( asm_str, 0x400000, &model, 0, 0, error );


    if ( r <= 0 ) {
        printf( "%s", error );
        return 0;
    }

    if ( model.length >= 1 ) {
        printf( "    " );
        for ( i=0; i < r; i++ )
            printf( "%02X ", model.code[i] );
    } else {
        printf( "assembled to 0 length instruction" );
        return 0;
    }

    return 1;
}

void on_error( char * msg ) {

    printf( "%s", msg );
    exit( 0 );
}

int hex_to_byte( uint8_t h, uint8_t l ) {
    
         if ( l >= '0' && l <= '9' ) l =  l-'0';
    else if ( l >= 'a' && l <= 'f' ) l = (l-'a')+10;
    else if ( l >= 'A' && l <= 'F' ) l = (l-'A')+10;
    else
        on_error( "invalid hex value" );

         if ( h >= '0' && h <= '9' ) h =  h-'0';
    else if ( h >= 'a' && h <= 'f' ) h = (h-'a')+10;
    else if ( h >= 'A' && h <= 'F' ) h = (h-'A')+10;
    else
        on_error( "invalid hex value" );
    
    return (h<<4) | l;
}

int disassemble( uint8_t * str ) {
    
    uint8_t error[ 1024 ] = { '\0' };
    t_disasm model;
    int i=0, j=0, r=0;
    int l=0;

    uint8_t bytes[ 1024 ] = { '\0' };
    memset( bytes, 0, sizeof( bytes ) );

    while ( 1 ) {

        uint8_t h = str[j];
        uint8_t l = str[j+1];
        j+=2;

        if ( h == '\0' ) break;
        if ( l == '\0' ) break;
        
        bytes[ i ] = hex_to_byte( h, l );
        
        while ( str[j] == ' ' ) j++;

        i++;
    }

    r = Disasm( bytes, i, 0x400000, &model, DISASM_CODE );
    printf( "    %i %s %s\n", r, model.dump, model.result );

    return 1;
}

int main( int argc, uint8_t ** args ) {
    
    uint8_t * mode = 0;

    if ( argc <= 2 ) {
        printf( "missing arguments" );
        return -1;
    }

    mode = args[1];

    switch ( *mode ) {
    case ( 'a' ):
        if ( assemble( args[2] ) )
            return 0;
        break;

    case ( 'd' ):
        if ( disassemble( args[2] ) )
            return 0;
        break;

    default:
        printf( "unknown mode\n" );
    }

    return -1;
}
